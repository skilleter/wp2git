#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

################################################################################
# Script to copy a Wordpress web site backup created with Updraft from a
# directory synced from Google drive, unpack it into the git working tree,
# commit and upload it.
################################################################################

# Who to send notifications to

email="git@skilleter.org.uk"

# Enable debug

enable_debug=false

config=''
configdir=''

# Name of the site (as embedded in the backup filenames)

SITENAME=""

# Exit trap

trap 'echo "--------------------------------------------------------------------------------"' EXIT

################################################################################
# Exit with an error message

function error()
{
   echo -e >&2 "ERROR:\n\n$@"

   echo -e "subject: $config - wp2git error (directory=$configdir)\n$@" | sendmail $email

   exit 1
}

################################################################################
# Exit with a success message

function success()
{
   echo -e >&2 "SUCCESS:\n\n$@"

   echo -e "subject: $config - wp2git completed successfully (directory=$configdir)\n$@" | sendmail $email

   exit 0
}

################################################################################
# Output a debug message

function debug()
{
   if $enable_debug
   then
      echo "[[$@]]"
   fi
}

################################################################################
# Entry point

echo "*******************************************************************************"
echo "* Running $@ at $(date)"
echo "*******************************************************************************"

# Defaults - anything not listed here must be in the configuration file

args=$(getopt --name "$0" --options "v" --longoptions "verbose" -- "$@") || exit 1
eval "set -- $args"

until [[ $1 == "--" ]]
do
   case $1 in
      '-v' | '--verbose'  ) 
         enable_debug=true 
         ;;		     
   esac
   shift
done
shift

# Read the configuration file

if [[ $# == 0 ]]
then
  error "You must specify the configuration file"
fi

# Check for matching file, matching file with .cfg extension
# then for same thing in script directory

config="$1"
configdir=$(dirname "$0")
found=false

for conf in "$config" "$config.cfg" "$configdir/$config" "$configdir/$config.cfg"
do
   if [[ -f "$conf" ]]
   then
      source "$conf"
      found=true
      break
   fi
done

if ! $found
then
   error "Missing configuration '$config'"
fi

# Check that the variables we need have been specified

[[ -z "${GIT_WORKING_TREE:-}" ]]            && error "GIT_WORKING_TREE not specified"
[[ -z "${GIT_WORKING_TREE_DIR:-}" ]]        && error "GIT_WORKING_TREE_DIR not specified"

if [[ ! -d "$GIT_WORKING_TREE/$GIT_WORKING_TREE_DIR" ]]
then
   if ! mkdir -p "$GIT_WORKING_TREE/$GIT_WORKING_TREE_DIR"
   then
      error "Invalid GIT_WORKING_TREE_DIR - '$GIT_WORKING_TREE_DIR'"
   fi
fi

[[ -z "${SOURCE_DIR:-}"           ]] && error "SOURCE_DIR not specified"
[[ ! -d "${SOURCE_DIR}"           ]] && error "Missing SOURCE_DIR - '$SOURCE_DIR'"

# Version can be specified as an optional second parameter

shift
if [[ $# == 1 ]]
then
   version="*$1*"
else
   version="*"
fi

# Move to the source directory

cd "$SOURCE_DIR"

echo "--------------------------------------------------------------------------------"
echo "   GIT_WORKING_TREE     = $GIT_WORKING_TREE"
echo "   GIT_WORKING_TREE_DIR = $GIT_WORKING_TREE_DIR"
echo "   SOURCE_DIR           = $SOURCE_DIR"
echo "   SITENAME             = $SITENAME"
echo "   Version              = $version"
echo "--------------------------------------------------------------------------------"

# Get the most backup recent file

last_file=$(ls -rt "$SOURCE_DIR/"${version}*${SITENAME}*db.gz 2>/dev/null | tail -n1) || true

[[ -z "$last_file" ]] && error "No files found matching '${version}*${SITENAME}*.zip' in '$SOURCE_DIR/'"

debug "Most recent backup file='$last_file'"

# Strip the category suffix

name_prefix=$(basename "${last_file%-*.gz}")

debug "Name prefix='$name_prefix'"

# Move to the git working tree

cd "$GIT_WORKING_TREE" || error "Can't locate git working tree at $GIT_WORKING_TREE"

if [[ ! -d .git ]]
then
   error "$GIT_WORKING_TREE is not a Git working tree!"
fi

# Delete the previous backup

debug "Deleting & recreating $GIT_WORKING_TREE_DIR from $GIT_WORKING_TREE"

if ! rm -rf "$GIT_WORKING_TREE_DIR" 
then
   echo "Error deleting previous $GIT_WORKING_TREE_DIR - moving it out of the way"
   mv "$GIT_WORKING_TREE_DIR" "$GIT_WORKING_TREE_DIR-$$"
fi

mkdir -p "$GIT_WORKING_TREE_DIR" || error "Unable to create directory $GIT_WORKING_TREE_DIR"
cd "$GIT_WORKING_TREE_DIR"

# Unpack the new backup

for backup in "$SOURCE_DIR/$name_prefix"*.zip
do
   debug "Unzipping '$backup'"

   unzip -q "$backup" || error "Error unpacking '$backup'"
done

# Add the database

database_dump_file="database.sql"

debug "Copying and unpacking ${name_prefix}-db.gz into '$database_dump_file'"

database="$SOURCE_DIR/${name_prefix}-db.gz"

cd ..
gunzip --stdout "$database" > "$database_dump_file" || error "Error unpacking database backup '$database'"

git add --all "$database_dump_file" "$GIT_WORKING_TREE_DIR" || error "Unable to add updated files to git"

# If nothing's changed since last time, just exit

if [[ -z $(git status --porcelain) ]]
then
   success "No changes since last backup"
fi

# Commit and push the changes

git commit -a -m "Updated on $(date) with backup: $name_prefix" || error "Unable to commit updated files into git"

for remote in $(git remote)
do
   echo "Pushing updates to $remote"
   git push --all $remote || error "Unable to push updates to $remote"
done

echo "Running malware scan"

malware=$(clamscan --infected --recursive --detect-pua=yes --block-macros=yes . 2>&1 || true)
echo -e "$malware"

success "Status:\n\n$(git diff --stat HEAD^)\n\nMalware status:\n\n$malware"

exit 0
